This is a helper tool that returns the number of parameters required by certain Windows(R) API.
The executable already contains a copy of the API database containing 17849 items as per
http://www.msfn.org/board/topic/154868-importpatcher-find-and-fix-dependency-problems/page__view__findpost__p__989120
(full topic http://www.msfn.org/board/topic/154868-importpatcher-find-and-fix-dependency-problems/ )
An updated ini file may be placed in the same folder. If the name is other than APIParameterCount.ini, load the new file by
choosing 'Change DB' from the tray menu.

To use, either run with API name as parameter (i.e. APC __argc) to get the parameter count in a message box,
or launch directly, to be presented with a list of API names and their parameters.

The list will also be presented if the API name provided as parameter cannot be found in the list of known APIs.

Double-click on a list item will copy API name to clipboard (same as clicking 'Copy name' button).

The source code is included in the package. It is written in AutoHotkey Basic/Classic 1.0.48.05 (ANSI) and it will only compile with that version.
For details see http://www.autohotkey.com

© Drugwash, March-April 2012
This is free (open-source) software

Change log

1.0.0.0
- initial version

1.0.1.0
- added parameter count display (in view of future Add/Edit/Delete mode)
- added tray menu option to save database (cleaned out of duplicates and sorted)
- fixed sorting bug
- replaced original database with a clean one, without duplicates, named APIParameterCount.ini

1.0.2.0
- added Add/Edit ability. Changes will be saved to file upon clicking 'Save file' button. No 'Delete' function yet
- added button to copy count to clipboard
- fixed progressbar not following loading progress
- changed database file type to UNIX (LF) as opposed to DOS (CR+LF), to save a little space (395kB -> 378kB)
- changed section name in database from 'APIParameterCounts' to 'APIParameterCount', for consistency with application's name
