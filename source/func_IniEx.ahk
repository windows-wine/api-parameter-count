; couple of ini file manipulation functions
; that supplement Win9x 64kB limitation
; � Drugwash April 18, 2012, v1.2
; formerly used in DDT

IniReadEx(file, section, key, default="ERROR")
{
IfExist, %file%
	FileRead, ini, %file%
else if !file
	return default, ErrorLevel := 1
else ini := file	; assume file is the name of a variable
if !(s := InStr(ini, "[" section "]"))
	return
StringTrimLeft, ini, ini, s+StrLen("[" section "]`n")-1
Loop, Parse, ini, `n, `r
	{
	if (SubStr(A_LoopField,1,1)="[" && SubStr(A_LoopField,0,1)="]")
		return default
	 else if InStr(A_LoopField, key "=") = 1
		{
		StringTrimLeft, r, A_LoopField, % StrLen(key)+1
		r = %r%
		return r
		}
	}
return default
}

IniWriteEx(var, ByRef file, section, key, write=1)
{
IfExist, %file%
	FileRead, ini, %file%
else if (!file && write="1")
	return, ErrorLevel := 1
else if write=1
	ini=
else if write != 1
	ini := file	; assume file is the name of a variable
if !(s := InStr(ini, "[" section "]"))
	nfile := "[" section "]`n" key "=" var "`n"
else
	{
	StringLeft, nfile, ini, s+StrLen("[" section "]`n")-2
	StringTrimLeft, ini, ini, s+StrLen("[" section "]`n")-1
	Loop, Parse, ini, `n, `r
		{
		if (InStr(A_LoopField, key "=") = 1)
			{
			StringReplace, ini, ini, %A_LoopField%, %key%=%var%
			i=1
			break
			}
		else if (SubStr(A_LoopField,1,1)="[" && SubStr(A_LoopField,0,1)="]")
			{
			StringReplace, ini, ini, %A_LoopField%, %key%=%var%`n%A_LoopField%
			i=1
			break
			}
		}
	nfile .= "`n" ini
	if !i
		nfile .= key "=" var "`n"
	}
if !nfile
	msgbox, error: empty variable nfile in %A_ThisFunc%
if !write
	{
	file := nfile
	return
	}
i := write=1 ? file : write
IfExist, %i%
	FileDelete, %i%
FileAppend, %nfile%, *%i%
return
}
; t="0" (search type 0=value, 1=section, 2=key)
; k="" (all keys), s="" (all sections), returns section name(s)
; k and s can be multiple strings separated by |
IniFindEx(var, file, t="0", k="", s="")
{
IfExist, %file%
	FileRead, ini, %file%
else if !file
	return, ErrorLevel := 1
else ini := file	; assume file is the name of a variable
IfNotInString, ini, %var%
	return
k := k ? "|" k "|" : ""
s := s ? "|" s "|" : ""
r=
Loop, Parse, ini, `n, `r
	{
	if (SubStr(A_LoopField,1,1)="[" && SubStr(A_LoopField,0,1)="]")
		lastsect := SubStr(A_LoopField, 2, StrLen(A_LoopField)-2)
	IfNotInString, A_LoopField, %var%
		continue
	if (t="1" && var=lastsect)
		return lastsect
	StringLeft, key, A_LoopField, % InStr(A_LoopField, "=")-1
	StringTrimLeft, val, A_LoopField, % InStr(A_LoopField, "=")
	if (!t && InStr(val, var) && (!k OR InStr(k, "|" key "|")) && (!s OR InStr(s, "|" lastsect "|")))
		r .= lastsect "`n"
	else if (t="2" && var=key)
		r .= lastsect "`n"
	}
StringTrimRight, r, r, 1
return r
}
